﻿/*
 * Copyright 2016 Dieter Lunn
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows.Input;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using TheAirline2.Common.Events;
using TheAirline2.Data;
using TheAirline2.Menus.Views;
using WPFLocalizeExtension.Engine;

namespace TheAirline2.ViewModels
{
    [Export]
    public class MainWindowViewModel : BindableBase
    {
        private readonly AirlineDbContext _dbContext;
        private readonly IEventAggregator _eventAggregator;
        private readonly LocalizeDictionary _localizeDictionary = LocalizeDictionary.Instance;
        private readonly IRegionManager _regionManager;

        [ImportingConstructor]
        public MainWindowViewModel(IEventAggregator eventAggregator, IRegionManager regionManager,
            AirlineDbContext dbContext)
        {
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _dbContext = dbContext;

            LoadedCommand = new DelegateCommand(Loaded);
            ChangeLanguageCommand = new DelegateCommand(ChangeLanguage);
        }

        public ICommand LoadedCommand { get; set; }
        public ICommand ChangeLanguageCommand { get; set; }

        private void ChangeLanguage()
        {
            var langDialog = ServiceLocator.Current.TryResolve<DialogLanguage>();
            _eventAggregator.GetEvent<ShowDialogEvent>().Publish(langDialog);
        }

        private void Loaded()
        {
            var lang = _dbContext.Settings.FirstOrDefault()?.Language;

            if (string.IsNullOrEmpty(lang))
            {
                ChangeLanguage();
            }
            else
            {
                _localizeDictionary.SetCultureCommand.Execute(lang);
            }

            _regionManager.RequestNavigate("MainRegion", new Uri("/MenuMain", UriKind.Relative));
        }
    }
}