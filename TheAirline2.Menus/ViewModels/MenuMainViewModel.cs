﻿/*
 * Copyright 2016 Dieter Lunn
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

using System.ComponentModel.Composition;
using System.Windows.Input;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using TheAirline2.Common.Events;
using TheAirline2.Menus.Views;

namespace TheAirline2.Menus.ViewModels
{
    [Export]
    public class MenuMainViewModel : BindableBase
    {
        private readonly IEventAggregator _eventAggregator;

        [ImportingConstructor]
        public MenuMainViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }

        public ICommand NewGameCommand => new DelegateCommand(NewGame);

        private void NewGame()
        {
            var dialog = ServiceLocator.Current.TryResolve<DialogNewGameWizard>();
            _eventAggregator.GetEvent<ShowDialogEvent>().Publish(dialog);
        }
    }
}