﻿/*
 * Copyright 2016 Dieter Lunn
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Linq;
using MaterialDesignThemes.Wpf;
using Prism.Commands;
using Prism.Mvvm;
using TheAirline2.Data;
using TheAirline2.Data.Models;
using WPFLocalizeExtension.Engine;

namespace TheAirline2.Menus.ViewModels
{
    [Export]
    public class DialogLanguageViewModel : BindableBase
    {
        private readonly AirlineDbContext _dbContext;
        private readonly LocalizeDictionary _dictionary = LocalizeDictionary.Instance;

        [ImportingConstructor]
        public DialogLanguageViewModel(AirlineDbContext dbContext)
        {
            _dbContext = dbContext;
            _dictionary.IncludeInvariantCulture = false;
        }

        public IEnumerable<CultureInfo> LanguagesCollection => _dictionary.MergedAvailableCultures;
        public DelegateCommand<string> ChangeLanguageCommand => new DelegateCommand<string>(ChangeLanguage);

        private void ChangeLanguage(string lang)
        {
            _dictionary.SetCultureCommand.Execute(lang);

            var settings = _dbContext.Settings.FirstOrDefault();
            if (settings != null)
            {
                settings.Language = lang;
            }
            else
            {
                settings = new Settings {Language = lang};
                _dbContext.Settings.Add(settings);
            }

            _dbContext.SaveChanges();

            DialogHost.CloseDialogCommand.Execute(true, null);
        }
    }
}