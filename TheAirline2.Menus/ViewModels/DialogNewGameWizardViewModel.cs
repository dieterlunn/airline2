﻿using System.ComponentModel.Composition;
using MaterialDesignThemes.Wpf;
using Prism.Commands;
using Prism.Mvvm;
using TheAirline2.Data.Models;

namespace TheAirline2.Menus.ViewModels
{
    [Export]
    public class DialogNewGameWizardViewModel : BindableBase
    {
        public Player Player { get; set; } = new Player();
        public DelegateCommand CancelCommand => new DelegateCommand(Cancel);

        private void Cancel()
        {
            DialogHost.CloseDialogCommand.Execute(null, null);
        }
    }
}