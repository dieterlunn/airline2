﻿/*
 * Copyright 2016 Dieter Lunn
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

using System.ComponentModel.Composition;
using TheAirline2.Menus.ViewModels;

namespace TheAirline2.Menus.Views
{
    /// <summary>
    ///     Interaction logic for DialogNewGame.xaml
    /// </summary>
    [Export]
    public partial class DialogNewGameWizard
    {
        public DialogNewGameWizard()
        {
            InitializeComponent();
        }

        [Import]
        public DialogNewGameWizardViewModel ViewModel
        {
            get { return DataContext as DialogNewGameWizardViewModel; }

            set { DataContext = value; }
        }
    }
}