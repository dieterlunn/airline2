﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace TheAirline2.Common.Converters
{
    public class EnumToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var parameterString = parameter as string;

            if (string.IsNullOrEmpty(parameterString))
            {
                return DependencyProperty.UnsetValue;
            }

            if (Enum.IsDefined(value.GetType(), value) == false)
            {
                return DependencyProperty.UnsetValue;
            }

            var parameterValue = Enum.Parse(value.GetType(), parameterString);

            return parameterValue.Equals(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var parameterString = parameter as string;

            return string.IsNullOrEmpty(parameterString) ? DependencyProperty.UnsetValue : Enum.Parse(targetType, parameterString);
        }
    }
}