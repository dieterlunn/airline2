The Airline 2
===========

## Description
Do you think you can run an airline? Now is your chance. Start your own airline at the dawn of air travel or takeover an established airline and see where it can take you. Will you soar into the skies or crash land? Your destiny is yours to make.

## License
Released under the [Apache 2.0 License](http://www.apache.org/licenses/LICENSE-2.0.html)

## Download
You can download the source code at http://bitbucket.org/dieterlunn/airline2/overview

> Written with [StackEdit](https://stackedit.io/).