﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TheAirline2.Data;

namespace TheAirline2.Data.Migrations
{
    [DbContext(typeof(AirlineDbContext))]
    [Migration("20160805040953_AddPlayers")]
    partial class AddPlayers
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("TheAirline2.Data.Models.Player", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<int>("Region");

                    b.HasKey("Id");

                    b.ToTable("Players");
                });

            modelBuilder.Entity("TheAirline2.Data.Models.Settings", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Language");

                    b.HasKey("Id");

                    b.ToTable("Settings");
                });
        }
    }
}
