﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TheAirline2.Data;

namespace TheAirline2.Data.Migrations
{
    [DbContext(typeof(AirlineDbContext))]
    [Migration("20160720145533_AddSettings")]
    partial class AddSettings
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("TheAirline2.Data.Models.Settings", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Language");

                    b.HasKey("Id");

                    b.ToTable("Settings");
                });
        }
    }
}
